package item2;

public class Square extends SuperShape {
      Square(double h, double w) {
    	  height = h;
          width = w;
      }
      @Override
      public double area(){
    	  return height * width;
       }
}